# -*- coding: utf-8 -*-

import logging
import os
import datetime

from gi.repository import Gdk
from gi.repository import Gedit
from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Pango
from gi.repository.Gtk import TextWindowType
from gi.repository import PeasGtk

from .config import Config
from .signals import Signals

APP_NAME = 'textviewprefs'
FILE_DIR = os.path.dirname(__file__)

# DEV
logging.basicConfig(level=logging.DEBUG)

class TextviewPrefsAppActivatable(GObject.Object, Gedit.AppActivatable):

    __gtype_name__ = 'MarginsAppActivatable'

    app = GObject.property(type=Gedit.App)

    def do_activate(self):
        '''Plugin activation'''
        TextviewPrefsWindowActivatable.config = Config({
            'schema_name': 'org.gnome.gedit.plugins.textviewprefs',
            'schema_dir': os.path.join(FILE_DIR, 'data', 'schemas'),
            'ui_path': os.path.join(FILE_DIR, 'data', 'ui', 'config.ui'),
            'ids': ['fsvmargins_adj', 'hmargins_adj', 'vmargins_adj', 'linespacing_adj', 'maxchars_adj'],
            'domain': APP_NAME,
            'connect': True
        })

    def do_deactivate(self):
        '''Plugin deactivation'''
        TextviewPrefsWindowActivatable.config = None

class TextviewPrefsWindowActivatable(GObject.Object, 
                                    Gedit.WindowActivatable,
                                    PeasGtk.Configurable, 
                                    Signals):

    __gtype_name__ = 'MarginsWindowActivatable'

    window = GObject.property(type=Gedit.Window)
    config = None

    def __init__(self):
        GObject.Object.__init__(self)
        Signals.__init__(self)
        self.reset()
        self._logger = logging.getLogger(APP_NAME)
        self.notebook = None
        self.notebook_width = None
        assert self.config

    def do_create_configure_widget(self):
        '''Configuration screen'''
        return self.config.create_widget()

    def do_activate(self):
        '''Plugin activation'''
        # logger
        self._logger.debug('activate')
        # notebook
        self.notebook = None
        self.notebook_width = None
        # listeners
        self._connect(self.window, 'tab-added', self.on_tab_added)
        self._connect(self.window, 'window-state-event', self.on_state_changed)
        self.config.settings.connect('changed::vmargins', self.on_margins_changed)
        self.config.settings.connect('changed::hmargins', self.on_margins_changed)
        self.config.settings.connect('changed::linespacing', self.on_linespacing_changed)
        self.config.settings.connect('changed::maxchars', self.on_maxchars_changed)

        # set width and margins
        self.fullscreen = False
        self.charwidth = None
        self.maxchars = self.config.settings.get_int('maxchars')
        self.update_margins()
        self.set_all_margins()
        # set linespacing
        self.update_linespacing()
        self.set_all_linespacings()

    def do_deactivate(self):
        '''Plugin deactivation'''
        self._logger.debug('deactivate')
        self._disconnect_all()
        self.reset()
        self.set_all_margins()
        self.set_all_linespacings()

    def do_update_state(self):
        pass

    def on_state_changed(self, window, state):
        '''React to change of window state.'''
        if (state.new_window_state & Gdk.WindowState.FULLSCREEN):
            self.fullscreen = True
            self.fshmargins = self.compute_size()
            self.fsvmargins = self.config.settings.get_int('fsvmargins')
            self.set_all_margins()
        elif self.fullscreen:
            self.fullscreen = False
            self.set_all_margins()

    def on_tab_added(self, window, tab):
        '''Set prefs for new tabs.'''
        if self.notebook is None:
            self.notebook = tab.get_parent()
            self._connect(self.notebook, 'size-allocate', self.on_notebook_size_allocate)
        self.set_margins(tab.get_view())
        self.set_linespacing(tab.get_view())

    def on_notebook_size_allocate(self, panel, allocation):
        if allocation.width == self.notebook_width:
            return
        print('on_notebook_size_allocate() - width: ' + str(allocation.width))
        self.notebook_width = allocation.width
        if self.fullscreen:
            self.fshmargins = self.compute_size()
            self.fsvmargins = self.config.settings.get_int('fsvmargins')
        else:
            self.update_margins()
        self.set_all_margins()

    def on_maxchars_changed(self, settings, key, data=None):
        '''Listening to max width changes'''
        self.maxchars = self.config.settings.get_int('maxchars')
        self.update_margins()
        self.set_all_margins()

    def on_margins_changed(self, settings, key, data=None):
        '''Listening to margins changed'''
        self.update_margins()
        self.set_all_margins()

    def on_linespacing_changed(self, settings, key, data=None):
        '''Listening to line-height changes'''
        self.update_linespacing()
        self.set_all_linespacings()

    def reset(self):
        '''Reset state'''
        self.fullscreen = False
        self.fshmargins = 0
        self.fsvmargins = 0
        self.vmargins = 0
        self.hmargins = 0
        self.maxchars = 0
        self.gsize = 0
        self.linespacing = 0

    # line-height

    def update_linespacing(self):
        '''Linespace update'''
        self.linespacing = self.config.settings.get_int('linespacing')

    def set_all_linespacings(self):
        '''Set line-height in all views.'''
        for view in self.window.get_views():
            self.set_linespacing(view)

    def set_linespacing(self, view):
        view.set_pixels_inside_wrap(self.linespacing)
        view.set_pixels_below_lines(self.linespacing)

    # margins

    def update_margins(self):
        self.hmargins = self.config.settings.get_int('hmargins')
        self.chmargins = self.compute_size()
        self.vmargins = self.config.settings.get_int('vmargins')

    def get_char_width(self):
        '''Try to get current default font and calculate character width.'''
        if self.charwidth is not None:
            return self.charwidth
        try:
            view = self.window.get_active_view()
            # Get font description (code from "text size" plugin)
            context = view.get_style_context()
            description = context.get_font(context.get_state()).copy()
            # Get font metrics
            pango_context = view.get_pango_context()
            lang = Pango.Language.get_default()
            metrics = pango_context.get_metrics(description, lang)
            # Calculate char width in pixels
            width = metrics.get_approximate_char_width() / Pango.SCALE
            self._logger.debug('charwidth:' + str(width))
            self.charwidth = width
            return width
        except:
            return 12

    def compute_size(self):
        '''Compute optimal size of margins.'''
        self._logger.debug('compute_size')

        # textview geometry
        view = self.window.get_active_view()

        if view is None or self.maxchars == 0:
            return self.hmargins

        view_width = view.get_allocated_width()
        self._logger.debug("view_width: %s", str(view_width))

        text_width = self.get_char_width() * self.maxchars
        self._logger.debug('text_width: %s', str(text_width))

        margin = view_width - text_width
        if margin <= self.hmargins:
            return self.hmargins

        return int(margin / 2)

    def set_all_margins(self):
        '''Set margins width to self.margins in all views.'''
        for view in self.window.get_views():
            self.set_margins(view)

    def set_margins(self, view):
        '''Set margins width to self.margins in a view.'''
        hsize = self.chmargins
        vsize = self.vmargins
        if self.fullscreen:
            hsize = self.fshmargins
            vsize = self.fsvmargins
        # left margin only if necessary
        if self.fullscreen is False or view.get_show_right_margin() is False:
            view.set_left_margin(hsize)
        view.set_border_window_size(TextWindowType.RIGHT, hsize)
        view.set_border_window_size(TextWindowType.TOP, vsize)
        view.set_border_window_size(TextWindowType.BOTTOM, vsize)
