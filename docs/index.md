Title:   Gedit : Textview preferences
Date:    2015-07-12
Updated: 2020-02-19
Cats:    informatique
Tags:    gedit, configuration, plugin
Technos: python, gtk
State:   stable
Version: 1.2.0 (2020-02-19)
Download: /lab/gedit-textview-prefs/build/gedit-textview-prefs-1.2.0.tar.gz
Contact: true
Intro: ''Textview preferences'' est un plugin pour Gedit ≥ 3.30 qui permet de modifier certains paramètres dans l'affichage du texte : les marges ainsi que l'interligne.

°°|install°°
## Installation

..include:: /data/snippets/gedit-plugin-install.txt
    plugin-name = textview-prefs

°°|config°°
## configuration

°°pic pos-left sz-half°°
![Écran de configuration](/lab/gedit-textview-prefs/pics/config-1-2.jpg)

• **Line spacing** : space between lines.
• **Max chars** : Maximum characters in one line.
• **Horizontal margins** : Minimal left and right margin.
• **Vertical margins** : top and bottom margin.
• **Vertical margins** (fullscreen) : top and bottom margin in fullscreen mode.

## Exemple

°°pic pos-centre sz-full°°
----
![Sans préférences](/lab/gedit-textview-prefs/pics/without-prefs.png)
----
Without text prefs.

°°pic pos-centre sz-full°°
----
![Avec préférences](/lab/gedit-textview-prefs/pics/with-prefs.png)
----
With line spacing and margins.

°°pic pos-centre sz-full°°
----
![With max chars](/lab/gedit-textview-prefs/pics/max-chars.jpg)
----
With max chars.

°°|changelog°°
## Historique

..include::./changelog.md

## Ressources et références

Inspiré par [Zim Distraction Free plugin](http://zim-wiki.org/manual/Plugins/Distraction_Free_Editing.html), [Gedit-fullscreen-margins](https://github.com/sergejx/gedit-fullscreen-margins) et [Focus Writer](http://gottcode.org/focuswriter/).

..include:: /data/snippets/gedit-plugin-ref.txt

°°|licence°°
## Licence

..include::./licence.md
