°°changelog°°
next:
	* UPD: max width instead of max chars ?
	* FIXME: maxchars not updated when font is changed. Needs gedit restart.
1.2.0 (2020-02-19):
    * add: Largeur maximale du texte (maxchars)
1.1.1 (2019-11-27):
    * fix: Recalcul des marges lors de l'ouverture du panneau latéral.
1.1.0 (2019-10-20):
    * add: interligne
1.0.0 (2015-07-12):
    * add: première version (marges)
