#!/bin/bash

# Project archive build
# ======================

# Dossiers
CDIR="$( cd "$( dirname "$0" )" && pwd )"
BDIR=$CDIR"/build/"
SDIR=$CDIR"/src/"
DDIR=$CDIR"/docs/"

# Variables
APP_NAME='textview-prefs'
BUILD_NAME='gedit-'$APP_NAME

# builder
# -----------------------
# $1 Numéro de version
build() {
    echo "Build de "$APP_NAME
    
    local vname=$BUILD_NAME$1
    local archname=$vname".tar.gz"
    local vdir=$BDIR$vname"/"
    
    # création du dossier de build dédié
    mkdir $vdir
    
    # Supprimer l'archive si elle existe déjà
    if [[ -f $archname ]]; then
        rm $archname
    fi
    
    # copie du fichier plugin
    cp -p -t $vdir $SDIR$APP_NAME".plugin"
    
    # traitement du dossier plugin
    cp -prL -t $vdir $SDIR$APP_NAME
    
    # Supprimer les fichier *.pyc, dossiers __pycache__
    find $vdir -type f -name "*.pyc" -delete
    find $vdir$APP_NAME -type d -name "__pycache__" -delete
    
    # Création de l'archive tar.gz
    cd $BDIR
    tar -czf $archname $vname
    
    # Suppression du dossier de build
    rm -fr $vdir
}

# Helper
# -----------------------
helper() {
    echo -e "Construction d'une archive du plugin Gedit "$APP_NAME
    echo -e "Usage: build.sh <version>"
    echo -e "  <version>    Numéro de version."
}

# args 
# -----------------------
if [[ $1 ]]; then
    build "-"$1
else
    helper
fi
